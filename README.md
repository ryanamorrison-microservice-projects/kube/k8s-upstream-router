k8s-upstream-router
=========
A role to create an upstream router for a Kubernetes cluster.  The router runs FRR and assumes it will be talking to something using BGP (e.g., cilium with bgp control plane enabled).

Requirements
------------
No additional ansible requirements.  Assumes a host or VM with a single interface.

Role Variables
--------------
The following defaults were used in unit testing (molecule) and are intended to be overridden.
```

``` 

Dependencies
------------
This role is intended to be used with other kube roles to create a routed network and working load-balancer IP's however there are no specific dependencies between this role and the others.

Example Playbook
----------------
The following is an example playbook used in testing, also see the `molecule` folder.
```
---
- hosts: router
  become: yes
  become_method: sudo
  roles:
    - { role: k8s-upstream-router }
```

License
-------
BSD

Author Information
------------------
Ryan A. Morrison (ryan@ryanamorrison.com)
